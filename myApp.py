from waitress import serve
from werkzeug.middleware.proxy_fix import ProxyFix

from app import app

app.wsgi_app = ProxyFix(
    app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1
)

if __name__ == "__main__":
    # app.run(host='0.0.0.0',port=49161,debug=True)
    # cf: https://dev.to/thetrebelcc/how-to-run-a-flask-app-over-https-using-waitress-and-nginx-2020-235c
    serve(app, host='0.0.0.0', port=5000, url_scheme='https')
