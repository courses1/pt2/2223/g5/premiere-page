FROM python:3.11

EXPOSE 5000

WORKDIR /

COPY . .
RUN pip install -r requirements.txt

CMD python myApp.py
