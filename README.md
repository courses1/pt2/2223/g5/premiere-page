# Première Page

Source code of the project-game "Première Page" suggested by the "Eye Smart" community and with the bachelor course "Projet Transverse II", bachelor UNIGE-CUI. This web appication serves to provide some basic image editing tools with the goal of being used in activities that have a young audience. The users are invited to think about their own image and what feelings they wish to express through their editing.

## Getting Started

Web application:
- [HTML](https://www.w3.org/html/)
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html)
    - [Bulma.css](https://bulma.io/)
- [Javascript](https://www.javascript.com/)
    - [Cropper.js](https://fengyuanchen.github.io/cropperjs/)
    - [Splide.js](https://splidejs.com/)
    - [fabric.js](http://fabricjs.com/)
- [Python](https://www.python.org/)
    - [flask](https://flask.palletsprojects.com/en/2.2.x/)
    - [waitress](https://docs.pylonsproject.org/projects/waitress/en/stable/index.html#)
- [NGINX](https://nginx.org/en/)
- [Docker](https://www.docker.com/)

This web application is currently available publicly on this address: <TBA>
The docker image used for the deployement can be found here <TBA>

You can clone and download this repository with the following command:

    git clone git@gitlab.unige.ch:courses1/pt2/2223/g5/premiere-page.git

### Prerequisites

Requirements for running the web application locally: 
- [Python 3+](https://www.python.org/)
    - Do not forget to install the python requirements as well:

Do not forget to install the python requirements as well:

    pip install -r requirements.txt

or

    pip3 install -r requirements.txt

- Web-browser supporting Javascript language

Optional:
- Running [NGINX server](https://nginx.org/en/)

### Running the application:

Run the following command from the root repository: 

    python myApp.py


or 

    python3 myApp.py


If it didn't work, you can try to run it with: 

    flask run

Once the application is running, visit your localhost address (on port 5000). For example: 

    127.0.0.1:5000/

### Deployment 

An easy way to deploy this web application is by using [Docker](https://www.docker.com/). 

You can build a docker image and compose with the following command: 

    docker compose -f "docker-compose.yml" up -d --build 


You can run the Docker image:

    docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

Find the information needed for the configuration and the docker compose: 

    docker-compose.yml


## Authors

  - **Ali Mehdi** - *Developper*
  - **Codrut-Sorin Burciu** - *Developper*
  - **Julien Alexandre Döbeli** - *Developer*
  - **Lïna Souaci** - *Developper*
  - **Tom Pascal Chappuis** - *Developper*
<br><br/>
  - **Ashley Caselli** - *Course Assistant*
