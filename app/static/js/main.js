// Get the source image to be edited
let image = document.getElementById('sourceImage');

let croppedImage = document.getElementById(`output`);
let cropper;
var textEditor = new fabric.Canvas("c", { preserveObjectStacking:true});
textEditor.rotationCursor="grab";

// Get the canvas for the edited image
var canvas = document.getElementById('canvas');
canvas.width=300;
canvas.height=400;

// Get the 2D context of the image
var context = canvas.getContext('2d');

// Get all the sliders of the image
let brightnessSlider = document.getElementById("brightnessSlider");
let contrastSlider = document.getElementById("contrastSlider");
//let grayscaleSlider = document.getElementById("grayscaleSlider");
let hueRotateSlider = document.getElementById("hueRotateSlider");
let saturateSlider = document.getElementById("saturationSlider");
let whitebalanceSlider = document.getElementById("whitebalanceSlider")
let angleSlider = document.getElementById("angleSlider")
//let sepiaSlider = document.getElementById("sepiaSlider");

let filterValues = "";

let history=[];


let history_redo=[];

let last_couv=''

var step=0;

var secondCanvas = document.getElementById("myCanvas");
var thirdCanvas = document.getElementById("myCanvas3");


var ctx = secondCanvas.getContext("2d");
var ctx3 = thirdCanvas.getContext("2d");

var width = secondCanvas.width;
var height = secondCanvas.height;//width / ratio;

var width3 = thirdCanvas.width;
var height3 = thirdCanvas.height;


var imgInstance;

const del = document.querySelector('#delete')

document.getElementById("firstStep").style.display = "none";
document.getElementById("secondStep").style.display = "none";
document.getElementById("thirdStep").style.display = "none";
document.getElementById("fourthStep").style.display = "none";


//This function handle the upload with the cropper step. 
function uploadImage(event) {
	
	// Set the source of the image from the uploaded file
	image.src = URL.createObjectURL(event.target.files[0]);

	image.onload = function () {
		
		// Set the canvas the same width and height of the image
		if (cropper == null){
			image.src = URL.createObjectURL(event.target.files[0]);

			image.onload = function () {
				
				// Set the canvas the same width and height of the image
				
				cropper = new Cropper(image,{
					aspectRatio: 3/4,
					viewMode: 1,
					scale:1,
					background: false,
					modal: false,
					minContainerWidth: 300,
					minContainerHeight: 400,			
					
				});
				
				document.getElementById(`cropImageBtn`).addEventListener(`click`,
				function(){
					var croppedImage = cropper.getCroppedCanvas(
						{
						minWidth: 300,
						minHeight: 400,
						}
					).toDataURL("image/png");
					croppedImage.width=300;
					croppedImage.height=400;
					document.getElementById("output").style.height ="400px";
					document.getElementById("output").style.width ="300px";
					document.getElementById(`output`).src = croppedImage;
					
					document.getElementById("firstNext").style.display="block";
					
				});
				
			};
		
		document.getElementById("first-step-options").style.display = "block";
		document.getElementById("firstNext").style.display = "none";
		document.querySelector('.help-text').style.display = "none";
		document.querySelector('.file-controls').style.display = "block";
		document.querySelector('.image-controls').style.display = "flex";
		document.getElementById("preview-box").style.display = "block"

		// Show the image editor controls and hide the help text
		
		context.drawImage(croppedImage, 0, 0);	

		}
		else{
			image.src = URL.createObjectURL(event.target.files[0]);
			image.onload= function(){
				cropper.destroy();
				cropper=  new Cropper(image,{
					aspectRatio: 3/4,
					viewMode: 1,
					scale:1,
					background: false,
					modal: false,
					minContainerWidth: 300,
					minContainerHeight: 400,			
					
				});
				
				document.getElementById(`cropImageBtn`).addEventListener(`click`,
				function(){
					var croppedImage = cropper.getCroppedCanvas(
						{
						minWidth: 300,
						minHeight: 400,
						}
					).toDataURL("image/png");
					croppedImage.width=300;
					croppedImage.height=400;
					document.getElementById(`output`).src = croppedImage;
					
					document.getElementById("firstNext").style.display="block";
					
				});
			};
			
		};
			
};
}

//fonction pour pivoter l'image
function rotate(){
	cropper.rotate(-angleSlider)
	angleSlider=document.getElementById("rotationSlider").value
	cropper.rotate(angleSlider)
}

//fonction pour inverser verticalement l'image
function flipX(){
	if (cropper.imageData.scaleX==-1){
		cropper.scaleX(1)
	}
	else{
		cropper.scaleX(-1)
	}

}

//focntion pour réinitialiser le cropper
function resetCropper(){
	cropper.reset()
}


function history_applyFilter() {

	filterValues =
		"brightness(" + brightnessSlider.value + "%" +
		") contrast(" + contrastSlider.value + "%" +
		//") grayscale(" + grayscaleSlider.value + "%" +
		") saturate(" + saturateSlider.value + "%" +
		") hue-rotate(" + hueRotateSlider.value + "deg" + ")";

	context.filter = filterValues;

	history.push(
		{
		brightnessSlider:brightnessSlider.value,
		contrastSlider:contrastSlider.value,
		//grayscaleSlider:grayscaleSlider.value,
		hueRotateSlider:hueRotateSlider.value,
		saturateSlider:saturateSlider.value,
		whitebalanceSlider: whitebalanceSlider.value,
		}
	)
	context.drawImage(croppedImage, 0, 0, canvas.width, canvas.height);
	WhiteBalance(whitebalanceSlider.value,context,canvas);
	
}

// This function is used to update the image
// along with all the filter values
function applyFilter() {

	// Create a string that will contain all the filters
	// to be used for the image
	filterValues =
		"brightness(" + brightnessSlider.value + "%" +
		") contrast(" + contrastSlider.value + "%" +
		//") grayscale(" + grayscaleSlider.value + "%" +
		") saturate(" + saturateSlider.value + "%" +
		//") sepia(" + sepiaSlider.value + "%" +
		") hue-rotate(" + hueRotateSlider.value + "deg" + ")";

	// Apply the filter to the image
	context.filter = filterValues;

	// Draw the edited image to canvas

	context.drawImage(croppedImage,0, 0, canvas.width, canvas.height);
	WhiteBalance(whitebalanceSlider.value,context,canvas);
}

//fonction pour ajouter l'action faite dans l'historique d'actions
function historyPush(){
	history.push(
		{
		brightnessSlider:brightnessSlider.value,
		contrastSlider:contrastSlider.value,
		//grayscaleSlider:grayscaleSlider.value,
		hueRotateSlider:hueRotateSlider.value,
		saturateSlider:saturateSlider.value,
		whitebalanceSlider: whitebalanceSlider.value,
		}

);
	history_redo=[];
}

// Reset all the slider values to there default values
function resetImage() {
	brightnessSlider.value = 100;
	contrastSlider.value = 100;
	//grayscaleSlider.value = 0;
	hueRotateSlider.value = 0;
	saturateSlider.value = 100;
	whitebalanceSlider.value = 6600;
	//sepiaSlider.value = 0;
	applyFilter();
}

//fonction pour revenir en arrière
function backwards(){
	if (history.length>1){
		history_redo.push(history.pop());

		let previous_state=history.pop();
		brightnessSlider.value=previous_state.brightnessSlider;
		contrastSlider.value=previous_state.contrastSlider;
		//grayscaleSlider.value=previous_state.grayscaleSlider;
		hueRotateSlider.value=previous_state.hueRotateSlider;
		saturateSlider.value=previous_state.saturateSlider;
		whitebalanceSlider.value=previous_state.whitebalanceSlider;
		
		history_applyFilter();

	}
	
}
//fonction pour sauvegarder l'image
function saveImage() {
	var canvastemp = document.getElementById("canvas");
    var anchor = document.createElement("a");
    anchor.href = canvastemp.toDataURL("image/png");
    anchor.download = "premiere_page.PNG";
    anchor.click(); 

}

//fonction pour rétablir l'action
function forward(){
	if (history_redo.length>0){
		let next_state=history_redo.pop();
		brightnessSlider.value=next_state.brightnessSlider;
		contrastSlider.value=next_state.contrastSlider;
		//grayscaleSlider.value=next_state.grayscaleSlider;
		hueRotateSlider.value=next_state.hueRotateSlider;
		saturateSlider.value=next_state.saturateSlider;
		whitebalanceSlider.value=next_state.whitebalanceSlider;
		
		history_applyFilter();
	}

}

//fonction pour passer à l'étape suivante, selon l'étape dans laquelle on est
function nextStep(){
	if (step==0){
		step=step+1
		document.getElementById('zeroStep').style.display = "none";
		document.getElementById('firstStep').style.display = "block";
		
	}

	else if (step==1){

		step=step+1
		document.getElementById('firstStep').style.display = "none";
		document.getElementById('secondStep').style.display = "block";

		canvas = cropper.getCroppedCanvas({
			width: 300,
			height: 400,
		});
		
		canvas.crossOrigin = "anonymous";
		applyFilter();
		startImage();
	}

	else if (step==2){
		step=step+1
		document.getElementById('secondStep').style.display = "none";
		document.getElementById('thirdStep').style.display = "block";
		
		// Apply the filter to the image
		ctx.filter = filterValues;
		ctx3.filter = filterValues;
	
		WhiteBalance(whitebalanceSlider.value,ctx,secondCanvas);
		WhiteBalance(whitebalanceSlider.value,ctx3,thirdCanvas);
	}

	else if(step==3){
		step=step+1
		document.getElementById('thirdStep').style.display = "none";
		document.getElementById('fourthStep').style.display = "block";

		refrechSrc();
	}
}

//fonction pour revenir en arrière d'une étape
function previousStep(){
	if (step==1){
		step=step-1
		document.getElementById('zeroStep').style.display = "block";
		document.getElementById('firstStep').style.display = "none";
	}

	else if (step==2){
		step=step-1
		document.getElementById('firstStep').style.display = "block";
		document.getElementById('secondStep').style.display = "none";
	}

	else if (step==3){
		step=step-1
		document.getElementById('secondStep').style.display = "block";
		document.getElementById('thirdStep').style.display = "none";
	}

	else if (step==4){
		step=step-1
		document.getElementById('thirdStep').style.display = "block";
		document.getElementById('fourthStep').style.display = "none";

	}
}

//fonction pour établir le canvas avec l'image dessus pour l'étape 3
function startImage() {
    var img = new Image();

	img.src = image.src;
	img.onload = function () {

	ctx.clearRect(0,0,width, height);
	ctx3.clearRect(0,0,width3,height3);

	ctx.drawImage(croppedImage, 0, 0, width, height);
	ctx3.drawImage(croppedImage,0,0,width3,height3);

	};
}

//fonction pour appliquer la couverture sur l'image
function selectCouv(couv){

	var img = new Image();
	last_couv=couv.src;
	img.src = couv.src;
	
	img.onload = function () {
	
	ctx.clearRect(0,0,width, height);
	ctx3.clearRect(0,0,width3, height3);
	ctx.drawImage(croppedImage, 0, 0, width, height);
	ctx3.drawImage(croppedImage, 0, 0, width3, height3);
	WhiteBalance(whitebalanceSlider.value,ctx,secondCanvas);
	WhiteBalance(whitebalanceSlider.value,ctx3,thirdCanvas);
	ctx.filter = "none";
	ctx3.filter = "none";
	ctx.drawImage(img, 0, 0, width, height);
	ctx3.drawImage(img, 0, 0, width3, height3);
	ctx.filter = filterValues;
	ctx3.filter = filterValues;

	};
}

function downloadCouv() {
    if (step==3){
		var canvastemp = document.getElementById("myCanvas3");
	}
	else if (step==4){
		var canvastemp = document.getElementById("c3");

		var textEditortemp = new fabric.Canvas("c3", { preserveObjectStacking:true});
		textEditortemp.setHeight(3600);
		textEditortemp.setWidth(2700);
		img = new Image();
		img.src = thirdCanvas.toDataURL();
		
		canvastemp_ctx = canvastemp.getContext("2d")
		canvastemp_ctx.drawImage(thirdCanvas, 0, 0,2700,3600)

		textEditortemp.setBackgroundImage(img.src, textEditortemp.renderAll.bind(textEditortemp));
		imgInstance = new fabric.Image(thirdCanvas, {
			height: 3600,
			width: 2700,
			left:0,
			top:0,
			selectable: false,
			evented: false
		});
		textEditortemp.add(imgInstance)

		var objects = textEditor.getObjects().length;
		for (var i = 1;i<objects;i++){
			textEditortemp.add(textEditor.getObjects()[i])

			textEditortemp.getObjects()[i].setTop(textEditortemp.getObjects()[i].top*9)
			textEditortemp.getObjects()[i].setLeft(textEditortemp.getObjects()[i].left*9)
	
			textEditortemp.getObjects()[i].setScaleX(textEditortemp.getObjects()[i].scaleX*9)
			textEditortemp.getObjects()[i].setScaleY(textEditortemp.getObjects()[i].scaleY*9)
		};

		textEditortemp.renderAll();
		
	}
    
    var anchor = document.createElement("a");
    anchor.href = canvastemp.toDataURL("image/png");
    anchor.download = "premiere-page";
    anchor.click(); 
	if (step==4){

		textEditortemp.setWidth(0);
		textEditortemp.setHeight(0);
		textEditortemp.calcOffset();
		for (var i = 1;i<objects;i++){
			textEditortemp.getObjects()[i].setTop(textEditortemp.getObjects()[i].top/9)
			textEditortemp.getObjects()[i].setLeft(textEditortemp.getObjects()[i].left/9)
	
			textEditortemp.getObjects()[i].setScaleX(textEditortemp.getObjects()[i].scaleX/9)
			textEditortemp.getObjects()[i].setScaleY(textEditortemp.getObjects()[i].scaleY/9)
		};
	}

}

function WhiteBalance(temperatureblanc,ctxWB,canevasWB){

	//Clean the previous modification
	ctxWB.drawImage(croppedImage, 0, 0, canevasWB.width, canevasWB.height)

	//image 300x400 → 120 000 ; R G B T → 120 000 x 4 = 480 000 values.
	var ImageData = ctxWB.getImageData(0,0,canevasWB.width,canevasWB.height);
	var r,g,b;
	
	temperatureblanc /= 100;
	for (let i = 0; i < ImageData.data.length; i += 4) {
		
		if (temperatureblanc<=66){
			r = 255;
			g = Math.min(Math.max(99.4708025861 * Math.log(temperatureblanc) - 161.1195681661, 0), 255);
		}
		else {
			r = Math.min(Math.max(329.698727446 * Math.pow(temperatureblanc - 60, -0.1332047592), 0), 255);
			g = Math.min(Math.max(288.1221695283 * Math.pow(temperatureblanc - 60, -0.0755148492), 0), 255);
		}
		if (temperatureblanc>=66){
			b = 255;
		}
		else if (temperatureblanc<=19){
			b = 0;
		}
		else{
			b = temperatureblanc - 10;
			b = Math.min(Math.max(138.5177312231 * Math.log(b) - 305.0447927307, 0), 255);
		}
		ImageData.data[i]=ImageData.data[i]*(255/r);
		ImageData.data[i+1]=ImageData.data[i+1]*(255/g);
		ImageData.data[i+2]=ImageData.data[i+2]*(255/b);
	}
	
	ctxWB.putImageData(ImageData, 0, 0);
}

var retourfonction = function(id){return document.getElementById(id)};

//changement de la couleur du texte de l'object sélectionné (textbox)

document.getElementById('text-color').onchange = function() {
	textEditor.getActiveObject().setFill(this.value);
	textEditor.renderAll();
	};

	//changement de la couleur du fond du texte

	document.getElementById('text-bg-color').onchange = function() {
		textEditor.getActiveObject().setBackgroundColor(this.value);
		textEditor.renderAll();
	};
	
	//changement de la police du texte

	document.getElementById('font-family').onchange = function() {
		textEditor.getActiveObject().setFontFamily(this.value);
		textEditor.renderAll();
	};
	
	//changement de la taille de la police du texte

	document.getElementById('text-font-size').onchange = function() {
		textEditor.getActiveObject().setFontSize(this.value);
		textEditor.renderAll();
	};
		
	//alignement du texte

	document.getElementById('text-align').onchange = function() {
		textEditor.getActiveObject().setTextAlign(this.value);
		textEditor.renderAll();
	};
	
	//changement du style de la case cochée

radios5 = document.getElementsByName("fonttype"); 
for(var i = 0, max = radios5.length; i < max; i++) {
	radios5[i].onclick = function() {
		
		if(document.getElementById(this.id).checked == true) {
			if(this.id == "text-cmd-bold") {
				textEditor.getActiveObject().set("fontWeight", "bold");
				textEditor.renderAll();
			}
			if(this.id == "text-cmd-italic") {
				textEditor.getActiveObject().set("fontStyle", "italic");
				textEditor.renderAll();
			}
			if(this.id == "text-cmd-underline") {
				textEditor.getActiveObject().set("textDecoration", "underline");
				textEditor.renderAll();
			}

		} else {
			if(this.id == "text-cmd-bold") {
				textEditor.getActiveObject().set("fontWeight", "");
				textEditor.renderAll();
			}
			if(this.id == "text-cmd-italic") {
				textEditor.getActiveObject().set("fontStyle", "");
				textEditor.renderAll();
			}  
			if(this.id == "text-cmd-underline") {
				textEditor.getActiveObject().set("textDecoration", "");
				textEditor.renderAll();
			}
		}
}
}

function refrechSrc(){
	img = new Image();
	
	img.src = secondCanvas.toDataURL();
	textEditor.setBackgroundImage(img.src, textEditor.renderAll.bind(textEditor));
	if (textEditor.getObjects().length==0){
		var text_empty = new fabric.IText(' ', { 
			left: 0,
			top:   0,
			fontFamily: 'arial black',
			fill: '#333',
			fontSize: 1,
		},
	   )
	   text_empty.visible = false;
	   textEditor.add(text_empty)
	}

}

function Addtext() { 
	textEditor.add(new fabric.IText('Titre Couverture', { 
 		left: 150,
 		top: 200,
 		fontFamily: 'arial',
		fill: '#333',
		fontSize: 18,
		borderColor: 'Darkorange',
		cornerColor: 'black',
		cornerSize: 7,
		borderWidth: 20,
		strokeWidth: 20,
		transparentCorners: false,
		lineHeight: 1.04,
		padding: 3,


 	},
	));


}

function background_reset(){
	textEditor.getActiveObject().setBackgroundColor("");
	textEditor.renderAll();
}

function deleteText(){
	var remove_text = textEditor.getActiveObject() 
	textEditor.remove(remove_text);
}

del.addEventListener('click', () => {
	var objects = textEditor.getObjects();
	for(var i = 1; i < objects.length; i){   
	textEditor.remove(objects[i]);
	}textEditor.renderAll();
})

window.addEventListener("keydown", function (event) {
	if (event.key == "Delete") {
		deleteText();

  }
});

textEditor.on("object:selected", function(obj){
	document.getElementById("text-font-size").value=textEditor.getActiveObject().fontSize 

	document.getElementById("font-family").value=textEditor.getActiveObject().fontFamily;

	document.getElementById('text-color').value=textEditor.getActiveObject().fill
	if (textEditor.getActiveObject().backgroundColor!=null){
	document.getElementById('text-bg-color').value = textEditor.getActiveObject().backgroundColor;
	}
	else{
		document.getElementById('text-bg-color').value = ""
	}
	if (textEditor.getActiveObject().textAlign=="left"){
		document.getElementById("text-align").value="left";
	}
	else if (textEditor.getActiveObject().textAlign=="center"){
		document.getElementById("text-align").value="center";
	}
	else if (textEditor.getActiveObject().textAlign=="right") {
		document.getElementById("text-align").value="right";
	}

	if (textEditor.getActiveObject().fontWeight=="bold"){
		document.getElementById("text-cmd-bold").checked=true;
	}
	else{
		document.getElementById("text-cmd-bold").checked=false;
	}
	if (textEditor.getActiveObject().fontStyle=="italic"){
		document.getElementById("text-cmd-italic").checked=true;
	}
	else{
		document.getElementById("text-cmd-italic").checked=false;
	}
	if (textEditor.getActiveObject().textDecoration=="underline"){
		document.getElementById("text-cmd-underline").checked=true;
	}
	else{
		document.getElementById("text-cmd-underline").checked=false;
	}		
});

