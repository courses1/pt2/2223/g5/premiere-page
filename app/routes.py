from flask import render_template
from app import app
import os

@app.route('/')
@app.route('/index',methods=["GET"])
def index():
    path = sorted(os.listdir("./app/static/premierePage"))
    return render_template('index.html', title='Home', images= path)
